#ifndef PLAYER_CONTROLLER_H
#define PLAYER_CONTROLLER_H

#include <Audio/AudioSource.h>
#include <Graphics/Camera.h>
#include <Physics/Collider.h>
#include <Physics/Rigidbody.h>
#include <Script/Script.h>

#include "GameController.h"
#include "GrapplePoint.h"
#include "Animators/PlayerAnimator.h"

namespace GamePackage{
	class PlayerController : public PizzaBox::Script{
	public:
		PlayerController(PlayerAnimator* animator_, PizzaBox::AudioSource* walkSFX_, PizzaBox::AudioSource* grappleSFX_, PizzaBox::AudioSource* jumpSFX_, PizzaBox::AudioSource* landSFX_, PizzaBox::AudioSource* swingingSFX_, PizzaBox::AudioSource* splashSFX_);
		virtual ~PlayerController() override;

		virtual void OnStart() override;
		virtual void Update(const float deltaTime_) override;
		virtual void OnDestroy() override;
		virtual void OnCollision(const PizzaBox::CollisionInfo& other_) override;
		virtual void OnCollisionExit(PizzaBox::GameObject* other_) override;

		inline bool HasControl(){ return (!isDead && !hasWon); }

	private:
		GameController* gameController;
		PizzaBox::AudioSource* walkSFX;
		PizzaBox::AudioSource* grappleSFX;
		PizzaBox::AudioSource* jumpSFX;
		PizzaBox::AudioSource* landSFX;
		PizzaBox::AudioSource* swingingSFX;
		PizzaBox::AudioSource* splashSFX;
		PizzaBox::Camera* camera;
		PlayerAnimator* animator;
		PizzaBox::Rigidbody* rigidbody;
		PizzaBox::GameObject* grappleLine;
		GrapplePoint* currentGrapplePoint;

		bool isWalking;
		bool isSwinging;
		bool isSwitchingToSwinging;
		bool isDead;
		bool hasWon;

		float maxRotationPerSecond, MoveY;
		float pullSpeed, currentGrappleLength;
		float maxGrappleLength;
		float fallBooster;
		float deathTimer;

		void GroundMovement(float deltaTime_);
		void Swinging(float deltaTime_);

		void SwitchToSwinging();
		void SwitchToGroundMovement();
		
		GrapplePoint* FindNearestGrapple();
		bool IsOnGround();
	};
}

#endif //!PLAYER_CONTROLLER_H